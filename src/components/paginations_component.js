import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ArrowRight from '@material-ui/icons/ArrowRight';
import ArrowLeft from '@material-ui/icons/ArrowLeft';

const useStyles = makeStyles(theme => ({
	margin: {
		margin: theme.spacing(1)
	}
}));

const Pagination = props => {
	const classes = useStyles();
	return (
		<Fragment>
			<div>
				<Button
					onClick={props.backPage}
					variant="outlined"
					size="small"
					className={classes.margin}
				>
					<ArrowLeft></ArrowLeft>Atras
				</Button>
				<Button
					onClick={props.nextPage}
					variant="outlined"
					size="small"
					className={classes.margin}
				>
					Siguiente<ArrowRight></ArrowRight>
				</Button>
			</div>
		</Fragment>
	);
};

export default Pagination;
