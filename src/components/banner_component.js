import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Paper, Container, Hidden } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
	banner: {
		marginBottom: theme.spacing(4),
		backgroundImage: 'url(https://source.unsplash.com/user/erondu)',
		backgroundSize: 'cover',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center',
		height: '250px'
	}
}));

export default function Banner() {
	// styles
	const classes = useStyles();

	return (
		<Fragment>
			<CssBaseline />
			<Container maxWidth="lg">
				<main>
					<Hidden smDown>
						<Paper className={classes.banner} />
					</Hidden>
				</main>
			</Container>
		</Fragment>
	);
}
