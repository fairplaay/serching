import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { GridList, Button, IconButton, GridListTile, GridListTileBar } from '@material-ui/core';
import Pagination from './paginations_component';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		paddingTop: '32px'
	},
	gridList: {
		width: '90%'
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)'
	},
	button: {
		margin: theme.spacing(1),
		color: 'white'
	}
}));

function Images(props) {
	// styles
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<GridList cellHeight={180} cols={3} className={classes.gridList}>
				{props.images.map(tile => (
					<GridListTile key={tile.id}>
						<img src={tile.webformatURL} alt={tile.title} />
						<GridListTileBar
							title={tile.tags}
							subtitle={<span>by: {tile.user}</span>}
							actionIcon={
								<IconButton
									aria-label={`info about ${tile.tags}`}
									className={classes.icon}
								>
									<Button
										variant="outlined"
										className={classes.button}
										href={tile.largeImageURL}
										target="_blank"
									>
										Ver
									</Button>
								</IconButton>
							}
						/>
					</GridListTile>
				))}
			</GridList>
			<Pagination backPage={props.backPage} nextPage={props.nextPage} />
		</div>
	);
}

export default Images;
