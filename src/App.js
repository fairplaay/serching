import React, { useState, Fragment } from 'react';
import SearchBar from './components/search-bar_component';
import Banner from './components/banner_component';
import Images from './components/Images_component';
import AwesomeDebouncePromise from 'awesome-debounce-promise';
import { Collapse } from '@material-ui/core';

function App() {
	// state
	let [images, setImages] = useState([]);
	let [term, setTerm] = useState('');
	let [showBanner, setShowBanner] = useState(true);
	let [page, setPage] = useState('');

	const fectApi = () => {
		fetch(
			`https://pixabay.com/api/?key=12770928-943e017140bffb37afc472d77&q=${term}&per_page=12&page=${page}`
		)
			.then(res => res.json())
			.then(result => setImages(result.hits));
	};

	const search = search => {
		if (term.length === 1) {
			setImages([]);
			setShowBanner(false);
			console.log(images.length);
			console.log(term.length);
		}

		setTerm(search);
		setPage(1);
		AwesomeDebouncePromise(fectApi(term), 500);
	};

	const backPage = () => {
		if (page === 1) {
			return null;
		}
		setPage((page -= 1));
		fectApi(term);
		console.log(page);
	};

	const nextPage = () => {
		setPage((page += 1));
		fectApi(term);
		console.log(page);
	};

	const showImages = () => {
		if (images.length !== 0) {
			return <Images images={images} backPage={backPage} nextPage={nextPage} />;
		}
	};

	return (
		<Fragment>
			<SearchBar search={search} />
			<Collapse timeout={1000} in={showBanner}>
				<Banner />
			</Collapse>
			<Fragment>{showImages()}</Fragment>
		</Fragment>
	);
}

export default App;
